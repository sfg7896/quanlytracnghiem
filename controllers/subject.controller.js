const Subject = require('../models/subject.model');
const Class = require('../models/class.model');
const mongoose = require('../connection');
const db = mongoose.connection;
const jwt = require('jsonwebtoken');
const config = require('../_help/config.json');
var jwtDecode = require('jwt-decode');

//Get All Subject
exports.getAllSubject = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) { //all user
            infoUser = (jwtDecode(req.token));
            posts = await Subject.find();
            res.send({
                "status": true,
                "data": posts,
                "message": "!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

//Get All Subject not signed yet
exports.getAllSubjectNotSignedOfAccount = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) { //all user
            infoUser = (jwtDecode(req.token));
            idAccount = req.params.id;
            classes = await Class.find({ 'idAccount_Subject.idAccount': idAccount });
            idSubjectArray = []
            for(let cl of classes){
                idSubjectArray.push(cl.idAccount_Subject.idSubject);
            }
            posts = await Subject.find({ _id: { $nin:  idSubjectArray } });
            res.send({
                "status": true,
                "data": posts,
                "message": "!"
            });
        }
    }
    catch (e) {
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};

exports.createSubject = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            if (infoUser.sub.IdRole == 1) { //admin
                let subject = new Subject(
                    {
                        name: req.body.name,
                        credits: req.body.credits,
                    });
                await subject.save(function (err) {
                    if (err) {
                        console.log(err);
                    }
                    res.send({
                        "status": true,
                        "data": null,
                        "message": "Subject created successfully!"
                    });
                });
            }
            else {
                res.send({
                    "status": false,
                    "data": null,
                    "message": "You have no right to access!"
                });
            }
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
};
