const User = require('../models/user.model')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../_help/config.json');
var jwtDecode = require('jwt-decode');

//create user
exports.createUser = async function (req, res) {
}

//get user
exports.getUserByIdAccount = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            posts = await User.findOne({ 'idAccount': req.params.idAccount }).select('-profilePicture');
            res.send({
                status: true,
                data: posts,
                message: 'information of account'
            });

        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
}

//update user
exports.updateUser = async function (req, res) {
    try {
        if (jwt.verify(req.token, config.secret)) {
            infoUser = (jwtDecode(req.token));
            await User.findOneAndUpdate({ 'idAccount': req.body.idAccount }, req.body)
            res.send({
                "status": true,
                "data": '',
                "message": 'Updated'
            })
        }
    }
    catch (e) {
        console.log(e)
        res.send({
            "status": false,
            "data": e,
            "message": "Unidentified error!"
        });
    }
}