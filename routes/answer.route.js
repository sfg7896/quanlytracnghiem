const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const answer = require('../controllers/answer.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', answer.getAllAnswer);
router.post('/create', answer.createAnswer);
module.exports = router;
