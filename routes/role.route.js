const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const roleController = require('../controllers/role.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', roleController.getAllRole);
router.post('/create', roleController.createRole);
router.put('/update/:id', roleController.updateRole);
router.delete('delete/:id', roleController.deleteRole);
module.exports = router;
