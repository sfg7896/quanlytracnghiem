const express = require('express');
const router = express.Router();
const verifyToken = require('../_help/jwt.js');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const subject = require('../controllers/subject.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', verifyToken.verifyToken, subject.getAllSubject);
router.get('/notSigned/:id', verifyToken.verifyToken, subject.getAllSubjectNotSignedOfAccount);
router.post('/create', verifyToken.verifyToken, subject.createSubject);
module.exports = router;
