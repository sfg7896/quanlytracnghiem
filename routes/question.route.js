const express = require('express');
const router = express.Router();
const verifyToken = require('../_help/jwt.js');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const questionController = require('../controllers/question.controller');


// a simple test url to check that all of our files are communicating correctly.
router.post('/create', verifyToken.verifyToken, questionController.createQuestion);
router.get('/subject/:idSubject', verifyToken.verifyToken, questionController.getQuestionByIdSubject);
router.get('/testDetail/:idTestDetail', verifyToken.verifyToken, questionController.getQuestionByIdTestDetail);
router.delete('/delete/:id', verifyToken.verifyToken, questionController.deleteQuestion);
router.put('/update/:id', verifyToken.verifyToken, questionController.updateQuestion);
router.get('/:id', verifyToken.verifyToken, questionController.getQuestionById);
module.exports = router;