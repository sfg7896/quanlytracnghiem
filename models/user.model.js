const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    idAccount: {
        type: Schema.ObjectId,
        ref: 'account'
    },
    fullName: {
        type: String,
    },
    dayOfBirth: {
        type: String
    },
    profilePicture: {
        type: String,
    },
    address: {
        type: String,
    },
    phoneNumber: {
        type:  Number,
    }
});


// Export the model
module.exports = mongoose.model('User', UserSchema);
