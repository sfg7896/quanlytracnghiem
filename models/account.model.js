const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AccountSchema = new Schema({
    UserName: {
        type: String,
        require: true
    },
    Password: {
        type: String
    },
    Facebook: {
        type: String,
    },
    Gmail: {
        type: String,
    },
    IdRole: {
        type:  Number,
        ref: 'Role'
    }
});


// Export the model
module.exports = mongoose.model('Account', AccountSchema);
