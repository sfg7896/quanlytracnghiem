const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let testSchema = new Schema({
    idClass: {
        type: Schema.ObjectId,
        ref: "Class",
        require: true,
    },
    idTestDetail: {
        type: Schema.ObjectId,
        ref: "TestDetail",
        require: true,
    },
    result: {
        type: Number,
        },
    timeComplete: {
        type: Number,
    },
    isComplete: {
        type: Boolean,
    }
});

testSchema.index({ idClass: 1, idTestDetail: 1 }, { unique: true })

// Export the model
module.exports = mongoose.model('Test', testSchema);
