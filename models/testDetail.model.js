const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let TestDetailSchema = new Schema({
    listIdQuestion: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Question'
    }],
    idSubject:{
        type: mongoose.Schema.ObjectId,
        ref: 'Subject',
    },
    description: {
        type: String,
    },
    isOpen: {
        type: Boolean,
    },
    duration: {
        type: Number,
    },
    quantityOfQuestions: {
        type: Number,
    }
});


// Export the model
module.exports = mongoose.model('TestDetail', TestDetailSchema);
