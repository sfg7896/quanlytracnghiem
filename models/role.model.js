const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let RoleSchema = new Schema({
    _id: {
        type: Number,
    },
    Name: {
        type: String,
        require: true
    }
});


// Export the model
module.exports = mongoose.model('Role', RoleSchema);
