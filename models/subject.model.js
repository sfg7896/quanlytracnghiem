const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SubjectSchema = new Schema({
    name: {
        type: String
    },
    credits: {
        type: Number,
    },
});


// Export the model
module.exports = mongoose.model('Subject', SubjectSchema);
